package com.pnguyen.bookservice.command.api.data;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jdk.jfr.DataAmount;
import lombok.Data;

@Entity
@Table(name = "t_book")
@Data
public class Book {
    @Id
    private String id;
    private String name;
    private String author;
    private Boolean isReady;
}
